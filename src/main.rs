mod api;
mod daemon;
mod failed;
mod logins;
mod pinned;

use api::{ApiClient, ApiError};
use clap::{Parser, Subcommand};
use std::io::{stdin, stdout, BufRead, Write};
use ureq::Header;
use url::Url;

#[derive(Parser)]
#[clap(version, about, long_about = None)]
#[clap(propagate_version = true)]
struct Cli {
    #[clap(subcommand)]
    command: Command,
}

#[derive(Subcommand)]
enum Command {
    /// List and manage the saved logins to gitea services
    Logins {
        #[clap(subcommand)]
        cmd: Option<LoginsCommand>,
    },
    /// Run the notifications daemon
    Daemon,
}

#[derive(Subcommand)]
enum LoginsCommand {
    /// List all logins the applications knows about
    List {
        /// Include the tokens in the output
        #[clap(long)]
        tokens: bool,
    },
    /// Add a new login
    Add,
    /// Remove an existing login
    Remove,
}

fn main() {
    let cli = Cli::parse();

    match cli.command {
        Command::Daemon => daemon::run(),
        Command::Logins { cmd } => {
            if let Err(err) = logins_subcommand(cmd) {
                eprintln!("{err}");
                std::process::exit(1);
            }
        }
    }
}

fn logins_subcommand(subcommand: Option<LoginsCommand>) -> Result<(), String> {
    let existing = logins::get_logins().map_err(|e| format!("failed to get logins: {e}"))?;

    let subcommand = match subcommand {
        Some(subcommand) => subcommand,
        None => LoginsCommand::List { tokens: false },
    };

    match subcommand {
        LoginsCommand::List { tokens } => {
            if !tokens {
                for login in existing {
                    println!("{}", login.url);
                }
            } else {
                let max_url_length = existing
                    .iter()
                    .map(|l| l.url.as_str().len())
                    .max()
                    .unwrap_or(0);
                let max_url_length = usize::max(max_url_length, 3); // header has length 3
                let max_token_length = existing.iter().map(|l| l.token.len()).max().unwrap_or(0);
                let max_token_length = usize::max(max_token_length, 5); // header has length 5

                println!("{:max_url_length$} {:max_token_length$}", "URL", "TOKEN");
                println!("{0:=<max_url_length$} {0:=<max_token_length$}", "");
                for login in existing {
                    println!(
                        "{url:max_url_length$} {token}",
                        url = login.url,
                        token = login.token
                    );
                }
            }
            Ok(())
        }
        LoginsCommand::Add => {
            let stdin = stdin();
            let mut stdin = stdin.lock();

            let mut url = String::new();
            let mut token = String::new();

            // ask for URL
            print!("URL: ");
            stdout().flush().unwrap();
            stdin.read_line(&mut url).unwrap();
            let url = url.trim();
            let url = Url::parse(url).map_err(|e| format!("{url:?} is not a valid URL ({e})"))?;

            // ask for token
            println!(
                "Please enter a valid access token. You can obtain it from {}.",
                url.join("user/settings/applications").unwrap()
            );
            print!("Token: ");
            stdout().flush().unwrap();
            stdin.read_line(&mut token).unwrap();
            let token = token.trim().to_owned();

            // validate that the token parses into a valid header value
            if Header::new("", &token).value().is_none() {
                return Err(
                    "The token must only consist of visible ASCII characters (32-127)".to_owned(),
                );
            };

            let new_login = logins::Login { url, token };

            // validate that the new login grants access to the API
            let client = ApiClient::new(&new_login);
            match client.me() {
                Ok(user) => println!("successfully authenticated as user '{}'", user.login),
                Err(err) => {
                    match err {
                        ApiError::Networking(msg) => eprintln!(
                            "warning: could not validate login due to networking problems ({msg})"
                        ),
                        ApiError::Unauthorized => eprintln!(
                            "warning: authorization to the service failed using this token"
                        ),
                        ApiError::Other(msg) => eprintln!(
                            "warning: could not validate login due to unexpected problems ({msg})"
                        ),
                    }
                    // an error occurred, does the user want to continue?
                    print!("Continue anyway (y/n) ");
                    stdout().flush().unwrap();

                    let mut choice = String::with_capacity(2);
                    stdin.read_line(&mut choice).unwrap();

                    match choice.trim().to_lowercase().as_str() {
                        "y" | "yes" => (),
                        _ => return Err("Operation cancelled".to_owned()),
                    }
                }
            };

            // save all logins to 'logins.json'
            let mut logins = existing;
            logins.push(new_login);
            logins::save_logins(&logins)
        }
        LoginsCommand::Remove => {
            let stdin = stdin();
            let mut stdin = stdin.lock();

            let padding = (existing.len() as f64).log10().floor() as usize + 1;
            let url_padding = existing
                .iter()
                .map(|l| l.url.as_str().len())
                .max()
                .unwrap_or(0);

            for (index, login) in existing.iter().enumerate() {
                // get user information from the API or display potential errors
                let client = ApiClient::new(login);
                let user = client.me();
                let user_info = match &user {
                    Ok(user) => &user.login,
                    Err(ApiError::Networking(_)) => "networking error",
                    Err(ApiError::Unauthorized) => "authorization failed",
                    Err(ApiError::Other(_)) => "unexpected error",
                };

                println!(
                    "{index:>padding$}: {url:url_padding$}  ({user_info})",
                    url = login.url
                );
            }

            print!("Enter the number of the login that you want to remove: ");
            stdout().flush().unwrap();
            let mut number = String::new();
            stdin.read_line(&mut number).unwrap();

            let number = number.trim();
            let number: usize = number
                .parse()
                .map_err(|e| format!("{number:?} is not a valid number ({e})"))?;

            if number >= existing.len() {
                return Err(format!(
                    "The number mustn't be higher than the largest index (which is {highest})",
                    highest = (existing.len() - 1),
                ));
            }
            // save all logins to 'logins.json'
            let mut logins = existing;
            logins.remove(number);
            logins::save_logins(&logins)
        }
    }
}
