use notify_rust::{Notification, NotificationHandle, Urgency};

pub(crate) struct FailedNotification {
    failed_notification: Option<NotificationHandle>,
}

impl FailedNotification {
    pub(crate) fn new() -> FailedNotification {
        Self {
            failed_notification: None,
        }
    }

    pub(crate) fn show(&mut self) -> Result<(), String> {
        if self.failed_notification.is_none() {
            self.failed_notification = Some(Notification::new()
                .appname("Gitea Notifications")
                .urgency(Urgency::Critical)
                .summary("Failed to execute command!")
                .body("For details, have a look at the log output of the daemon. This message will disappear, when the problem is resolved.")
                .icon("io.gitea.Logo")
                .finalize()
                .show()
                .map_err(|e| format!("could not display notification: {e}"))?);
        }
        Ok(())
    }

    pub(crate) fn hide(&mut self) {
        if let Some(failed_notification) = self.failed_notification.take() {
            failed_notification.close();
        }
    }
}
