use directories::ProjectDirs;
use serde::{Deserialize, Serialize};
use std::fs::{create_dir_all, File};
use std::io::Read;
use std::path::PathBuf;
use ureq::Header;
use url::Url;

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Login {
    pub url: Url,
    pub token: String,
}

fn get_logins_file() -> Result<PathBuf, String> {
    let project_dirs = ProjectDirs::from("org.codeberg", "eladyn", "gitea-notifiers")
        .ok_or("failed to get home directory")?;

    let logins_file = project_dirs.config_dir().join("logins.json");

    Ok(logins_file)
}

pub fn get_logins() -> Result<Vec<Login>, String> {
    let logins_file = get_logins_file()?;

    if logins_file.exists() {
        let mut file =
            File::open(logins_file).map_err(|e| format!("failed to open 'logins.json': {e}"))?;

        let mut content = String::new();
        file.read_to_string(&mut content)
            .map_err(|e| format!("failed to read from file: {e}"))?;

        if !content.trim().is_empty() {
            let logins: Vec<Login> = serde_json::from_str(&content)
                .map_err(|e| format!("logins.json is not valid: {e}"))?;
            // validate that the tokens are valid `HeaderValue`s
            let logins: Vec<Login> = logins
                .into_iter()
                .filter(|login| {
                    Header::new("", &login.token).value().or_else(|| {
                        eprintln!(
                            "The token {token:?} for URL {url} didn't contain only visible ascii characters (32-127), ignoring",
                            token = login.token,
                            url = login.url
                        );
                        None
                    }).is_some()
                })
                .collect();

            Ok(logins)
        } else {
            Ok(Vec::new())
        }
    } else {
        Err(format!(
            "no login file found at '{}'",
            logins_file.display()
        ))
    }
}

pub fn save_logins(logins: &[Login]) -> Result<(), String> {
    let logins_file = get_logins_file()?;
    create_dir_all(logins_file.parent().unwrap())
        .map_err(|e| format!("failed to create config directory: {e}"))?;

    let file =
        File::create(logins_file).map_err(|e| format!("could not create config file: {e}"))?;
    serde_json::to_writer_pretty(file, logins)
        .map_err(|e| format!("could not serialize logins: {e}"))?;

    Ok(())
}
