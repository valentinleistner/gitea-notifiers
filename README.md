# gitea-notifiers

A simple program that continuously requests unread and pinned notifications from the configured [Gitea][gitea-io]-Instances and displays them as desktop notifications.
Its primary target is linux, but it might also work on other platforms (PRs to improve this are welcome).

## Features

`gitea-notifiers` tries to be as reliable as possible. Its main goals are:

- robust to temporary failures (dbus, network)
- minimal resource usage
- no restarts needed / can run forever (logins can be added or removed dynamically)

It currently offers support for the following features:

- pinned notifications
- unread notifications
- multi-account support
- open in browser by clicking on a notification

## Installation

To build the project, you need a working [rust installation][rustup]:

```bash
# install the release binary
cargo install --git https://codeberg.org/eladyn/gitea-notifiers.git
```

## Usage

The application needs to be told about the Gitea instances that you want to receive notifications for.
This can be done with the CLI interface using the `gitea-notifiers logins` subcommand.

To start the daemon that continuously checks for new notifications, run `gitea-notifiers daemon`.

## Autostart on Login

To start the program automatically on login using `systemd` user services, you can copy / link the [`contrib/gitea-notifiers.service`](contrib/gitea-notifiers.service) file to one of `systemd`'s user unit search paths[^1].
After enabling it, the program should be started once your graphical environment is ready.

```bash
# make sure the service path exists
mkdir -p ~/.config/systemd/user/
# download the service file to the systemd folder
wget -O ~/.config/systemd/user/gitea-notifiers.service https://codeberg.org/eladyn/gitea-notifiers/raw/branch/main/contrib/gitea-notifiers.service
# enable the unit and start it immediately
systemctl --user enable --now gitea-notifiers.service

# check that everything is working
systemctl --user status gitea-notifiers.service
```

[^1]: See `systemd.service(5)`, *User Unit Search Path*

[gitea-io]: https://gitea.io/
[rustup]: https://rustup.rs/
